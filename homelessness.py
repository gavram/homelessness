import tabulator
import datetime
import dataflows
import csv

current_year = datetime.datetime.now().year

# Try to find what is the year with data available
try:
    with Stream('https://www.hudexchange.info/resources/documents/2007-'+ str(current_year)\
                + '-PIT-Counts-by-State.xlsx', sheet=current_year - 1, headers=1) as stream:
        last_year = current_year
        print(last_year)
except:
    last_year = current_year - 1

# Defining headers for two CSV files
header_row = ['year', 'state', 'sheltered', 'unsheltered', 'total']
header_row_us = ['year', 'sheltered', 'unsheltered', 'total']

# create CSV file to store data by states
with open('./data/data.csv','w', newline='\n') as csvfile:   
    writer = csv.writer(csvfile, delimiter=',')
    writer.writerow([g for g in header_row])

# create CSV file to store summary data by year
with open('./data/us_data.csv','w', newline='\n') as usfile:   
    writer = csv.writer(usfile, delimiter=',')
    writer.writerow([g for g in header_row_us]) 

# Obtaining data, full ETL
for year in range(2007, last_year+1):
    with tabulator.Stream('https://www.hudexchange.info/resources/documents/2007-'+ str(last_year) + '-PIT-Counts-by-State.xlsx',\
                 sheet=str(year), headers=1) as stream:
        for row in stream.iter():
            if row[0] != 'Total' :
                try:
                    row[2]*1
                    with open('./data/data.csv','a', newline='') as csvfile:
                        output_list = [str(year), 
                                       str(row[0] or ''), 
                                       str(row[6] or '').replace('.', ''),
                                       str(row[7] or '').replace('.', ''),
                                       str(row[2] or '').replace('.', '')]
                        csvfile.write(','.join(output_list) + '\n')
                except:
                    pass
            else:
                with open('./data/us_data.csv','a', newline='') as usfile:
                    output_list = [str(year), 
                                   str(row[6] or '').replace('.', ''),
                                   str(row[7] or '').replace('.', ''),
                                   str(row[2] or '').replace('.', '')]
                    usfile.write(','.join(output_list) + '\n')
