# Homelessness

### Homelessness dataset for all US states in period 2007 to today
* Definition:
    * Homelessness is defined as living in housing that is below the minimum standard or lacks secure tenure.
    * People can be categorized as homeless if they are:
        * living on the streets (primary homelessness);
        * moving between temporary shelters, including houses of friends, family and emergency accommodation (secondary homelessness);
        * living in private boarding houses without a private bathroom and/or security of tenure (tertiary homelessness)
    * Definition of homelessness vary from country to country.

## Data

Data obtained from https://www.hudexchange.info
There are no missing values in the dataset and it is very consistent

## Preparation
Data is obtained using homelessness.py script from the aforementioned web address. Script downloads data directly from .xlsx file using Tabulator.
The data is originally in .xlsx format and the script extract, transforms and cleans data into two CSV files, data.csv and us_data.csv

## Usage
Just clone or copy this repo on your local computer and run homelessnes.py

## Files
* /data/data.csv            - complete dataset for all US states from 2007
* /data/us_data.csv         - summarized dataset by years
* /img/chart.png            - screenshot of d3.js chart
* visualization.html        - visualization of data
* homelessness.py           - Python script that obtain and process whole dataset
* README.md                 - this readme file

## License
CC0-1.0 https://creativecommons.org/publicdomain/zero/1.0/
Data and Python scripts are free to use and distribute

## Total Net Worth by Year - Chart
![](/img/chart.png)